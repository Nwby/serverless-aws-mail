### This project is a basic config for sending mail using AWS SES & Lambda. It relies on the Serverless framework. 

For this to work you will also need to create a file in the directory called: **secrets.json**

In the secrets.json file include this

```javascript
{
    "NODE_ENV": "dev",
    "EMAIL": "your.email@goes-here.com",
    "DOMAIN": "*"
}
```

In production change domain to match your domain.